<?php

namespace Candy\Core;

Class Environment {
	
	static public $request;
	static public $app;
	static public $host;
	static public $instance;

	public function __construct() {

	}

	static public function init() {

		self::$request = (object) array(
			"uri" 		=> $_SERVER["REQUEST_URI"],
			"method"	=> strtolower($_SERVER["REQUEST_METHOD"]), 
			"params" 	=> array()
		);

		if ( self::$request->method == "post" )
			$request->params = $_POST;

		self::$host = (object) array(
			"root" => $_SERVER["DOCUMENT_ROOT"],
			"script" => $_SERVER["SCRIPT_NAME"]
		); 

		self::$app = (object) array(
			"uri" => self::getTrimUri()
		);
	}

	static public function factor() {

	}

	static public function getTrimUri() {

		$request_uri = explode("/", self::$request->uri);
		$script_name = explode("/", self::$host->script);

		foreach( $script_name as $key => $module ) {
			if ( $module == $request_uri[$key] )
				unset($request_uri[$key]);
		}

		return "/" . implode("/", $request_uri);
	}

}

?>