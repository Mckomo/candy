<?php

namespace Candy\Core;

Class Candy {

	protected $data;
	protected static $env;
	protected $config = array(); 

	public function __construct() {

		Environment::init();
		$this->router = new Router();

	}

	protected function request($method, $pattern, $callback) {

		$route = new Route($method, $pattern, $callback);
		$this->router->add($route);

	}

	// 
	public function get($pattern, $callback) {

		$this->request("get", $pattern, $callback);

	}

	public function post($pattern, $callback) {

		$this->request("post", $pattern, $callback);

	}

	public function put($pattern, $callback) {

		$this->request("put", $pattern, $callback);

	}

	public function delete($pattern, $callback) {

		$this->request("delete", $pattern, $callback);

	}

	public function run() {
		$content = $this->router->handle();
		//echo $content; 
	}

}

?>