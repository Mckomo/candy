<?php

namespace Candy\Core;

Class Router {

	/*
	*	Route list 
	*	@Route
	*
	*/

	protected $list;

	protected $request_route;

	public function __construct() {

		$this->list = array(
			"get" => array(),
			"post" => array(),
			"put" => array(),
			"delete" => array()
		);

		$this->request_route = new Route(
			Environment::$request->method,
			Environment::$app->uri
		);

	}


	public function add(Route $route) {
		$this->list[$route->method][] = $route; 
	}


	protected function find() {

		$request_method = $this->request_route->method;

		foreach ($this->list[$request_method] as $route) {
			if ( self::match( $this->request_route, $route ) )
				return $route;
		}

		return false;

	}

	// Match 2 routes together. One of routes has dynamic structure 
	static protected function match($route_static, $route_dynamic) {

		if ( $route_static->module_count != $route_dynamic->module_count ) 
			return false;
	

		foreach ($route_static->structure as $k => $module_static) {

			$module_dynamic = $route_dynamic->structure[$k];

			if ( $module_dynamic["type"] == "dynamic")
				continue;

			if ( $module_static["name"] != $module_dynamic["name"])
				return false;
		}

		return true;
	}
	
	// Extract callback function arguments from route structure 
	protected function extractParams($route) {

		$params = array();

		foreach ($route->structure as $k => $module) {
			if ( $module["type"] == "dynamic" )
				$params[$module["name"]] = $this->request_route->structure[$k]["name"];
		}

		return $params;
	}

	// 
	public function handle() {

		$route = $this->find();

		if( $route == false )
			Router::E404();

		$route->callback->function;
		$route->callback->args = $this->extractParams($route);
		call_user_func_array($route->callback->function, $route->callback->args);

	}
	
	private static function E404() {
		echo "Page canno't be found";
	}
}

?>