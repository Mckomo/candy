<?php

namespace Candy\Core;

class Route {
	

	public $method, $pattern;
	public $callback;
	public $structure, $module_count;

	public function __construct($method, $pattern, $callback = null) {

		$this->method = $method;
		$this->pattern = $pattern;
		$this->callback = (object) array(
			'function' => $callback,
			'args' => array()
		);

		list($this->structure, $this->module_count) = $this->analyse($pattern);

	}

	// Analyse url pattern. Retrun Url structure and count number of structure modules
	public function analyse() {

		// Split url pattern to array
		$pattern = explode("/", $this->pattern);
		$count = count($pattern); 

		if ( $count ) {

			$structure = array();

			foreach ($pattern as $key => $module) {

				// Checks if module is dynamic 
				if (preg_match("/^:/", $module) ) {
					$module = substr($module, 1);
					$structure[] = array(
						"type" => "dynamic",
					);
				} else {
					$structure[] = array(
						"type" => "static"
					);
				}

				$structure[$key]["name"] = $module;
				
			}

			array_shift($structure);
			return array($structure, $count);

		} else 
			throw new LogicException("Wrong url pattern");


	}

	// Returns Route instance
	public static function factor($url) {

		$route = new Route();
		return $route;

	}
}

?>